import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { Authenticate } from '@app/_models/authenticate';
import { AccountService } from '@app/_services';
import { AddComponent } from '@app/expense/add/add.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { useraccess } from '@app/_models/useraccess';
import { ChangepasswordComponent } from '@app/users/changepassword/changepassword.component';
import { MessagingService } from '@app/_services/messaging.service';
import { ListComponent } from '@app/expense/list/list.component';


@Component({
  selector: 'app-authorized',
  templateUrl: './authorized.component.html',
  styleUrls: ['./authorized.component.scss']
})
export class AuthorizedComponent implements OnInit {
  user: Authenticate;
  access: useraccess;
  isExpanded = false;
  showSubmenu: boolean = false;
  isShowing = false;
  showSubSubMenu: boolean = false;
  
  @ViewChild(ListComponent) child:ListComponent;

  IsPermissionMASTER: boolean;
  IsPermissionTodoEdit: boolean;
  IsPermissionTimeAnalysisView: boolean;
  IsPermissionTaskAnalysisView: boolean;
  IsPermissionOfficeTimelineView: boolean;
  IsPermissionEmployeeListView: boolean;
  IsPermissionTodoListView: boolean;
  message;
  constructor(
    private accountService: AccountService,
    public dialog: MatDialog,
    private router: Router,
    private messagingService: MessagingService
  ) {
    this.accountService.user.subscribe(x => this.user = x);
    this.accountService.userrights.subscribe(x => this.access = x);
  }

  get isMobileView(): boolean {
    return false;
  }
  ngOnInit(): void {
    if (this.user.changePassword) {
      this.openRoute('/account/firstlogin');
    }

    this.messagingService.requestPermission();
        this.messagingService.receiveMessage();
        this.message = this.messagingService.currentMessage;
  }

  ngAfterViewInit() {
   // this.child = new ListComponent;

  }

  logout() {
    this.accountService.logout();
  }

  openRoute(routeCommand: string) {
    this.router.navigate([routeCommand]);
  }

  mouseenter() {
    if (!this.isExpanded) {
      this.isShowing = true;
    }
  }

  mouseleave() {
    if (!this.isExpanded) {
      this.isShowing = false;
    }
  }

  menutoggel() {
    console.log('toggel');
    if (this.isExpanded == false) {
      this.isExpanded = true;
    } else {
      this.isExpanded = false;
    }
  }

  newExpense() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.height = '80%';
    dialogConfig.width = '80%';
    dialogConfig.data = {

    };

    const dialogRef = this.dialog.open(
      AddComponent,
      dialogConfig
    );
    dialogRef.afterClosed().subscribe(res => {
      this.child.loadData();
      if (res) {
        console.log('onClose', res);
      }
    });
  }

  openPassword(uid: string) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    // dialogConfig.height = '80%';
    // dialogConfig.width = '80%';
    dialogConfig.data = {
      id: uid,
      heading: 'Change Password',
      type: 2
    };

    const dialogRef = this.dialog.open(
      ChangepasswordComponent,
      dialogConfig
    );
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        console.log('onClose', res);
      }
    });
  }

}

