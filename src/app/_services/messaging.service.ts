import { Injectable } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { BehaviorSubject } from 'rxjs';
import { AlertService } from './alert.service';
import { NotificationService } from './notification.service';

@Injectable()
export class MessagingService {

  currentMessage = new BehaviorSubject(null);

  constructor(private angularFireMessaging: AngularFireMessaging, private alertService: AlertService, private notifivationservice: NotificationService) {
    this.angularFireMessaging.messages.subscribe(
      (_messaging: AngularFireMessaging) => {
        _messaging.onMessage = _messaging.onMessage.bind(_messaging);
        _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
      });
  }

  requestPermission() {
    this.angularFireMessaging.requestToken.subscribe(
      (token) => {
        console.log(token);
        this.notifivationservice.add(token).subscribe(data => {
          console.log(data);
        });
      });
  }

  receiveMessage() {
    this.angularFireMessaging.messages.subscribe(
      (msg) => {
        var title;
        var body;
        try {
          title = msg['notification'].title;
          body = msg['notification'].body;
        } catch (error) {
          
        }
        this.alertService.notification(title,body);
        console.log("show message!", msg);
        this.currentMessage.next(msg);
      });
  }
}
