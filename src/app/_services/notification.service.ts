import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { userToken } from '@app/_models/usertoken';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  baseURL = environment.apiUrl;

  public flag: Observable<userToken>;
  constructor(private http: HttpClient) {

  }

  add(token: string){
    var param = new userToken();
    param.token = token;
    return this.http.post(`${this.baseURL}notification`, param);
  }
}
