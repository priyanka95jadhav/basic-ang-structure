﻿import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '@environments/environment';
import { Authenticate } from '@app/_models/authenticate';
import { User } from '@app/_models/user';
import { useraccess } from '@app/_models/useraccess';
import { menu } from '@app/_models/menu';
import { userPermitions } from '@app/_models/userpermitions';

@Injectable({ providedIn: 'root' })
export class AccountService {
    private userSubject: BehaviorSubject<Authenticate>;
    private useraccess: BehaviorSubject<useraccess>;
    public user: Observable<Authenticate>;
    public userrights: Observable<useraccess>;
    baseURL = environment.apiUrl;
    constructor(
        private router: Router,
        private http: HttpClient
    ) {
        this.userSubject = new BehaviorSubject<Authenticate>(JSON.parse(localStorage.getItem('user')));
        this.useraccess = new BehaviorSubject<useraccess>(JSON.parse(localStorage.getItem('access')));
        this.user = this.userSubject.asObservable();
        this.userrights = this.useraccess.asObservable();
    }

    public get userValue(): Authenticate {
        return this.userSubject.value;
    }

    login(username, password) {
        return this.http.post<Authenticate>(`${this.baseURL}authenticate`, { username, password })
            .pipe(map(user => {

                
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('user', JSON.stringify(user));
                this.userSubject.next(user);
                return user;
            }));
    }

    logout() {
        // remove user from local storage and set current user to null
        localStorage.removeItem('user');
        localStorage.removeItem('access');
        this.userSubject.next(null);
        this.useraccess.next(null);
        this.router.navigate(['/account/login']);
    }
    
    accessRight(id: string){
        return this.http.get<useraccess>(`${this.baseURL}UserRight/${id}`).pipe(map(access => {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('access', JSON.stringify(access));
            this.useraccess.next(access);
            return access;
        }));
    }

    setRights(model: any){
        return this.http.post<useraccess>(`${this.baseURL}UserRight`,model);
    }

    permitions(id: string){
        return this.http.get<userPermitions>(`${this.baseURL}UserRight/${id}`);
    }

    menus(){
        return this.http.get<menu>(`${this.baseURL}Menus`);
    }
    
    register(user: User) {
        return this.http.post(`${this.baseURL}user`, user);
    }

    getAll() {
        return this.http.get<User>(`${this.baseURL}user`);
    }

    getById(id: string) {
        return this.http.get<User>(`${this.baseURL}user/${id}`);
    }

    update(id, params) {
        return this.http.put(`${this.baseURL}user/${id}`, params)
            .pipe(map(x => {
                // update stored user if the logged in user updated their own record
                if (id == this.userValue.id) {
                    // update local storage
                    const user = { ...this.userValue, ...params };
                    localStorage.setItem('user', JSON.stringify(user));

                    // publish updated user to subscribers
                    this.userSubject.next(user);
                }
                return x;
            }));
    }

    delete(id: string) {
        return this.http.delete(`${this.baseURL}user/${id}`)
            .pipe(map(x => {
                // auto logout if the logged in user deleted their own record
                if (id == this.userValue.id) {
                    this.logout();
                }
                return x;
            }));
    }

    changePassword(model: any){
        return this.http.post(`${this.baseURL}user/changepassword`,model);
        
    }
}