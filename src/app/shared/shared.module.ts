import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoaderComponent } from './loader/loader.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { FooterComponent } from './footer/footer.component';
import { MaterialImportModule } from '@app/material-import/material-import.module';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: 
  [
    LoaderComponent, 
    ConfirmDialogComponent, 
    FooterComponent
  ],
  imports: [
    CommonModule,
    MaterialImportModule,
    RouterModule
  ],
  exports:[
    LoaderComponent,
    ConfirmDialogComponent,
    FooterComponent
  ]
})
export class SharedModule { }
