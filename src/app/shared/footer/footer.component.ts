import { Component, OnInit } from '@angular/core';
import { FooterButton } from '@app/_models/FooterButton';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  searchQuery: string;
  version: string;
  constructor() { }

  ngOnInit(): void {
    this.version='1.0.1';
  }

  onButtonClicked(button: FooterButton) {
    if (button && button.onClick) {
      if (button.type === "Refresh") {
        this.searchQuery = "";
      }

      button.onClick();
    }
  }

}
