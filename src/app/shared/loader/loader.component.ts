import { Component, OnInit, Input } from '@angular/core';
import { LoaderService } from 'src/app/_services/loader/loader.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.less']
})
export class LoaderComponent implements OnInit {

  showLoader: boolean;
  message: string;
  constructor(private service: LoaderService) { }

  ngOnInit() {
   
    this.service.showLoader.subscribe(value => {
      setTimeout(() => { 
        this.showLoader = value;
      });
    });
    this.service.message.subscribe(value =>{ 
      setTimeout(() => { 
        this.message = value;
      });
    });

  }

}
